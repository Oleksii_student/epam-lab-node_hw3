const passwordGenerator = require('generate-password')
const nodemailer = require('nodemailer')

const getNewGeneratedPassword = () => {
  return passwordGenerator.generate({
    length: 10,
    numbers: true
  });
}

const sendEmailWithNewPassword = async (email, password) => {
  console.group('sendEmailWithNewPassword')
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
      secureConnection: false,
    service: 'gmail',
    auth: {
      user: process.env.EMAIL_LOGIN.trim(),
      pass: process.env.EMAIL_PASSWORD.trim()
    }
  })

  console.groupEnd()

  try {

    return await transporter.sendMail({
      from: process.env.EMAIL_LOGIN,
      to: email,
      subject: 'New password',
      text: 'New password',
      html: `<b>Your new password: <br> ${password}</br>`
    })
  } catch (e) {
    console.log('ERRORR =====>', e)
  }

}

module.exports = {
  getNewGeneratedPassword,
  sendEmailWithNewPassword
}
