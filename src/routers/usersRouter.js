const express = require('express');
const router = express.Router();

const { getProfileUser, editPasswordUser, deleteProfileUser } = require('../controllers/usersController.js');
const { passwordMiddleware } = require('../middlewares/passwordMiddleware.js');
const { asyncWrapper } = require('../asyncWrapper')


router.get('/', asyncWrapper(getProfileUser)); 

router.delete('/', asyncWrapper(deleteProfileUser));

router.patch('/password', asyncWrapper(passwordMiddleware), asyncWrapper(editPasswordUser));

module.exports = {
    usersRouter: router,
};