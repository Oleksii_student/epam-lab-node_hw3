const express = require('express');
const router = express.Router();

const { registerUser, loginUser, forgotPasswordUser } = require('../controllers/authContriller.js');
const { asyncWrapper } = require('../asyncWrapper');

router.post('/register', asyncWrapper(registerUser));

router.post('/login', asyncWrapper(loginUser));

router.post('/forgot_password', asyncWrapper(forgotPasswordUser));


module.exports = {
    authRouter: router,
};