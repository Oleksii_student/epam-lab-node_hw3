const express = require('express');
const morgan = require('morgan');
const dotenv = require('dotenv');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();

dotenv.config();
mongoose.connect(process.env.MONGODB_URI);

const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/usersRouter');
const { trucksRouter } = require('./routers/trucksRouter');
const { loadsRouter } = require('./routers/loadsRouter');

const { asyncWrapper } = require('./asyncWrapper');

const { authMiddleware } = require('./middlewares/authMiddleware');

const {
  driverRoleMiddleware,
} = require('./middlewares/driverRoleMiddleware');

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));
app.use(express.urlencoded({ extended: false }));

app.use('/api/auth', authRouter);
app.use('/api/users/me', asyncWrapper(authMiddleware), usersRouter);
app.use('/api/loads', asyncWrapper(authMiddleware), loadsRouter);
app.use('/api/trucks', asyncWrapper(authMiddleware), asyncWrapper(driverRoleMiddleware), trucksRouter);

function errorHandler(err, req, res) {
  return res.status(400).json({ message: err.message });
}

app.use(errorHandler);

const start = async () => {
  try {
    app.listen(process.env.PORT);
    console.log(`App started at port: ${process.env.PORT}`);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
