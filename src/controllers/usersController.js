const bcrypt = require("bcryptjs");

const {
  getUserByID,
  findUserByIDAndDelete,
  findUserByIDAndUpdate
} = require('../services/usersService');


const getProfileUser = async (req, res, next) => {
  const { _id, email, role, created_date } = req.user;

  return res.status(200).json({
    user: {
      _id,
      role,
      email,
      created_date: created_date
    }
  })
}


const deleteProfileUser = async (req, res, next) => {
  const userID = req.user._id;
  console.log('userID ===>>>', userID);

  await findUserByIDAndDelete(userID);

  return res.status(200).json({
    message: 'Profile deleted successfully'
  });
}


const editPasswordUser = async (req, res, next) => {
  let { _id, oldPassword, newPassword } = req.user;

  const { password: currentPassword } = await getUserByID(_id);

  const isPasswordCorrect = await bcrypt.compare(
    String(oldPassword),
    String(currentPassword)
  );

  if (!isPasswordCorrect) {
    throw Error('Invalid current password')
  }

  newPassword = await bcrypt.hash(newPassword, 10);

  await findUserByIDAndUpdate(_id, { password: newPassword });

  return res.status(200).json({
    message: 'Password changed successfully'
  });
}


module.exports = {
  getProfileUser,
  editPasswordUser,
  deleteProfileUser
};

